############
## README ##
############

Qt-based ROS catkin package template.
Required Qt5 version.

Modification based on qt-ros package (command line feature: catkin_create_qt_pkg) and Qt documentation site (http://qt.developpez.com/doc/5.0-snapshot/cmake-manual/)
